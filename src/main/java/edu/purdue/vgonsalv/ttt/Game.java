package edu.purdue.vgonsalv.ttt;

import static edu.purdue.vgonsalv.ttt.GameState.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Victor
 */
public class Game implements Serializable {

    private static final long serialVersionUID = 219587249771348092L;

    private GameState state;
    private int moves;
    private Peice winner;
    private final int size;
    private final Peice empty;
    private final Peice[] players;
    private final List<Peice> unmodifiablePlayers;
    private final Board board;
    private final FinalBoard unmodifiableBoard;
    private  int currentPlayerIndex;
    private transient boolean stateDirty;

    public Game(int size, Peice empty) {
        this.size = size;
        this.empty = empty;
        this.board = new Board(size, empty);
        this.unmodifiableBoard = new FinalBoard(board);
        for (int i = 0; i < size * size; i++) {
            board.setPeice(i, empty);
        }
        state = RESET;
        players = new Peice[2];
        unmodifiablePlayers = Collections.unmodifiableList(Arrays.asList(players));
        stateDirty = false;
        currentPlayerIndex = 0;
    }

    public Game(Game game) {
        this.size = game.size;
        this.empty = game.empty;
        this.board = new Board(game.board);
        this.unmodifiableBoard = new FinalBoard(board);
        this.state = game.getState();
        this.players = Arrays.copyOf(game.players, 2);
        unmodifiablePlayers = Collections.unmodifiableList(Arrays.asList(players));
        stateDirty = true;
        currentPlayerIndex = game.currentPlayerIndex;
        this.moves = game.moves;
    }

    public Board getBoard() {
        return unmodifiableBoard;
    }

    private Peice winningLine(int row, int col, int dr, int dc) {
        Peice first = board.getPeice(row, col);
        if (first == empty) {
            return empty;
        }
        for (row += dr, col += dc; row < size && col < size && row >= 0 && col >= 0; row += dr, col += dc) {
            if (board.getPeice(row, col) != first) {
                return empty;
            }
        }
        return first;
    }

    private GameState computeState() {
        if (moves < 2 * size-1) {
            return PLAYING;
        }
        //horizontal
        for (int row = 0; row < size; row++) {
            if ((winner = winningLine(row, 0, 0, 1)) != empty) {
                return WON;
            }
        }
        //VERTICAL
        for (int col = 0; col < size; col++) {
            if ((winner = winningLine(0, col, 1, 0)) != empty) {
                return WON;
            }
        }
        //diagonals
        if ((winner = winningLine(0, 0, 1, 1)) != empty
                || (winner = winningLine(size - 1, 0, -1, 1)) != empty) {
            return WON;
        }
        if (moves == size * size) {
            return TIE;
        } else {
            return PLAYING;
        }
    }

    public GameState getState() {
        if (stateDirty) {
            if (state == PLAYING) {
                state = computeState();
            }
            stateDirty = false;
        }
        return state;
    }

    public void reset() {
        if (state != RESET) {
            for (int i = 0; i < size * size; i++) {
                board.setPeice(i, empty);
            }
            state = RESET;
            currentPlayerIndex = 0;
            stateDirty = false;
        }
    }
    public void terminate(){
        this.state = TERMINATED;
        stateDirty = false;
    }
    public Peice getWinner() {
        return winner;
    }
    public boolean makeMove(int pos,int player){
        if( state == RESET){
            for (Peice p : players) {
                if(p== null || p == empty){
                    throw new IllegalStateException("Players not set");
                }
            }
            state = PLAYING;
        }else if(state != PLAYING ){
            return false;
        }
        if(board.getPeice(pos)!= empty){
            return false;
        }
        if(player!= currentPlayerIndex){
            return false;
        }
        board.setPeice(pos, players[player]);
        currentPlayerIndex = 1 - currentPlayerIndex;
        stateDirty = true;
        moves++;
        return true;
    }
    public void setPlayerPeice(int player, Peice peice){
        if(getState() != RESET){
            throw new IllegalStateException("Game must be reset to change player peices");
        }
        players[player] = peice;
    }
    
    public void setPlayerPeice(Peice ... peices){
        if(peices.length != 2){
            throw new IllegalArgumentException("only 2 players allowed");
        }
        for (int i = 0; i < 2; i++) {
            setPlayerPeice(i,peices[i]);
        }
    }
    public Peice getCurrentPlayer(){
        return players[currentPlayerIndex];
    }
    public int getCurrentPlayerIndex(){
        return currentPlayerIndex;
    }
    public List<Peice> getPlayers(){
        return unmodifiablePlayers;
    }
    public int getMoves(){
        return moves;
    }
    private void writeObject(ObjectOutputStream oos) throws IOException{
        getState();
        oos.defaultWriteObject();
        
    }
    
    public List<Integer> getAvailableMoves(){
        List<Integer> availMoves = new ArrayList<>();
        for (int i = 0; i < size * size; i++) {
            if(board.getPeice(i)==empty)
                availMoves.add(i);
        }
        return availMoves;
    }
}
