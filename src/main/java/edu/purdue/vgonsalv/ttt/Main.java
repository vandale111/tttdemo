package edu.purdue.vgonsalv.ttt;

import edu.purdue.vgonsalv.ttt.actor.Player;
import edu.purdue.vgonsalv.ttt.player.MinimaxPlayer;
import edu.purdue.vgonsalv.ttt.player.RandomPlayer;
import edu.purdue.vgonsalv.ttt.remote.RemoteObserverClient;
import edu.purdue.vgonsalv.ttt.remote.RemoteObserverServer;
import edu.purdue.vgonsalv.ttt.remote.RemotePlayer;
import edu.purdue.vgonsalv.ttt.remote.RemotePlayerServer;
import edu.purdue.vgonsalv.ttt.view.TextView;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Victor
 */
public class Main {

    public static void main(String[] args) throws ParseException, IOException {
        Option host = Option.builder("c").argName("hostname").hasArgs().build();
        Option observer = Option.builder("o").argName("hostname").hasArgs().build();
        Option observerServer = Option.builder("OS").argName("port").hasArgs().build();
        
        Options options = new Options();
        options.addOption(host).addOption(observer).addOption(observerServer);
        
        
        TextView tv = new TextView();
        
        CommandLineParser parser = new DefaultParser();
        CommandLine cl = parser.parse(options, args);
        final String[] remainingArgs = cl.getArgs();
        
        if(remainingArgs.length ==0){
            GameController game = new GameController(Arrays.asList(new MinimaxPlayer(),new RandomPlayer(1000)), Arrays.asList(tv));
            game.playGame();
            return;
        }
        int port = Integer.parseInt(remainingArgs[0]);
        Player player = new RandomPlayer(1000);
        ArrayList<Player> players = new ArrayList<>();
        RemoteObserverServer remoteObserverServer = null;
        if(cl.hasOption("OS")){
            int observerPort  = Integer.parseInt(cl.getOptionValue("OS"));
            remoteObserverServer = new RemoteObserverServer(observerPort, null);
            new Thread(remoteObserverServer).start();
        }
        if(cl.hasOption("c")){
            Player remote = new RemotePlayer(new Socket(cl.getOptionValue("c"),port));
            players.add(remote);
            players.add(new MinimaxPlayer());
        }else if(cl.hasOption("o")){
            RemoteObserverClient roc = new RemoteObserverClient(tv, new Socket(cl.getOptionValue("o"), port));
            roc.run();
            return;
        }else{
            RemotePlayerServer rps = new  RemotePlayerServer(port);
            players.add(player);
            players.add(rps.accept());
        }
        
        GameController game = new GameController(players, Arrays.asList(tv));
        if(remoteObserverServer!= null){
            remoteObserverServer.setGame(game);
        }
        game.playGame();
        if(remoteObserverServer != null){
            remoteObserverServer.shutdown();
        }
        
    }

}
