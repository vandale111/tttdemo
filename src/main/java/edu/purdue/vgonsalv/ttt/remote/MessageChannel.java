package edu.purdue.vgonsalv.ttt.remote;

import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.TerminatedMessage;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Victor
 */
public class MessageChannel implements Closeable,AutoCloseable{

    private final ObjectOutputStream out;
    private final ObjectInputStream in;

    public MessageChannel(OutputStream out, InputStream in) throws IOException {
        this(new ObjectOutputStream(out), new ObjectInputStream(in));
    }

    public MessageChannel(ObjectOutputStream out, ObjectInputStream in) {
        this.out = out;
        this.in = in;
    }

    MessageChannel(Socket socket) throws IOException{
        this(socket.getOutputStream(),socket.getInputStream());
    }

    public void sendMessage(Message message) throws IOException {
        System.out.println("Send: " + message);
        out.writeUnshared(message);
        out.flush();
    }

    public Message exchangeMessage(Message message) throws IOException{
        sendMessage(message);
        return receiveMessage();
    }

    public Message receiveMessage() throws IOException{
        Object obj;
        do {
            try {
                obj = in.readUnshared();
            } catch ( ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        } while (!(obj instanceof Message));
        System.out.println("Recv: "+ obj);
        return (Message) obj;
    }
    @Override
    public void close(){
        try{
            out.writeUnshared(new TerminatedMessage());
        }catch(IOException e){
            
        }
        try {
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(MessageChannel.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(MessageChannel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
