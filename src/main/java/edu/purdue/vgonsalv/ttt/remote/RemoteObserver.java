package edu.purdue.vgonsalv.ttt.remote;


import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.actor.Observer;
import edu.purdue.vgonsalv.ttt.message.ObserverMessage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author Victor
 */
public class RemoteObserver extends  Observer{
    private final MessageChannel mc;
    public RemoteObserver(Socket s)throws IOException{
        ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
        ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
        this.mc = new MessageChannel(oos, ois);
    }

    @Override
    public Message dispatchMessage(ObserverMessage message) {
        try{
            mc.sendMessage(message);
        }catch(IOException e){
            mc.close();
            throw new DisconnectException(e);
        }
        return Message.EMPTY;
    }
    
}
