package edu.purdue.vgonsalv.ttt.remote;

import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.actor.Player;
import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.MoveOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.MoveReplyMessage;
import edu.purdue.vgonsalv.ttt.message.ObserverMessage;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Victor
 */
public class RemotePlayer  extends Player implements Closeable,AutoCloseable{
    private final MessageChannel mc;
    public RemotePlayer(Socket socket) throws IOException {
        mc = new MessageChannel(socket);
    }

    @Override
    public Message dispatchMessage(ObserverMessage message) throws DisconnectException {
        try {
            switch(message.getType()){
                default:
                    return super.dispatchMessage(message);
                case MOVE_OCURRED:
                    MoveOcurredMessage moc = (MoveOcurredMessage) message;
                    if(moc.getPeice() != getPeice()){
                        mc.sendMessage(new MoveReplyMessage(moc.getPosition()));
                    }
                    return Message.EMPTY;
                case MOVE_REQUEST:
                    return mc.receiveMessage();
            }
        } catch (IOException ex) {
            throw new DisconnectException(ex);
        }
    }


    @Override
    public void close() {
       mc.close();
    }
}
