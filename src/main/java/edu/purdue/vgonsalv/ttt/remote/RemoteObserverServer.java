package edu.purdue.vgonsalv.ttt.remote;

import edu.purdue.vgonsalv.ttt.GameController;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Victor
 */
public class RemoteObserverServer implements Runnable {

    private GameController game;
    private final int port;
    private final AtomicBoolean running;
    private final ArrayList<RemoteObserver> buffeer;
    private ServerSocket ss;

    public RemoteObserverServer(int port, GameController game) throws IOException {
        this.port = port;
        running = new AtomicBoolean(true);
        this.game = game;
        this.buffeer = new ArrayList<>();
    }

    @Override
    public void run() {
        try (ServerSocket ss = new ServerSocket(port);) {
            this.ss = ss;
            while (running.get()) {
                Socket s = ss.accept();
                RemoteObserver obs = new RemoteObserver(s);
                synchronized (buffeer) {
                    if (game != null) {
                        game.addObserver(obs);
                    } else {
                        buffeer.add(obs);
                    }
                }
            }
        } catch (IOException ex) {
            if(running.getAndSet(false)){
                Logger.getLogger(RemoteObserverServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void shutdown() {
        running.set(false);
        if (ss != null) {
            try {
                this.ss.close();
            } catch (IOException ex) {
                Logger.getLogger(RemoteObserverServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public GameController getGame() {
        return game;
    }

    public void setGame(GameController game) {
        synchronized (buffeer) {
            if (this.game == null && game != null) {
                for (RemoteObserver remoteObserver : buffeer) {
                    game.addObserver(remoteObserver);
                }
            }
            this.game = game;
        }
    }

}
