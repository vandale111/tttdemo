package edu.purdue.vgonsalv.ttt.remote;

import edu.purdue.vgonsalv.ttt.Peice;
import edu.purdue.vgonsalv.ttt.actor.Player;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import edu.purdue.vgonsalv.ttt.Game;

/**
 *
 * @author Victor
 */
public class RemotePlayerServer {
    private final int port;
    public RemotePlayerServer(int port) {
        this.port = port;
    }
    
    public RemotePlayer accept() throws IOException{
        ServerSocket ss = new ServerSocket(port);
        return new RemotePlayer(ss.accept());
    }
}
