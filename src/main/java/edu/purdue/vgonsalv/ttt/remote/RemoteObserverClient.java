package edu.purdue.vgonsalv.ttt.remote;

import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.actor.Observer;
import edu.purdue.vgonsalv.ttt.message.ObserverMessage;
import edu.purdue.vgonsalv.ttt.message.TerminatedMessage;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Victor
 */
public class RemoteObserverClient implements Runnable {

    private final Observer observer;
    private final Socket socket;

    public RemoteObserverClient(Observer observer, Socket socket) throws IOException {
        this.observer = observer;
        this.socket = socket;
    }

    @Override
    public void run() {
        try (MessageChannel mc = new MessageChannel(socket)) {
            while (true) {
                Message message;
                message = mc.receiveMessage();
                if (message instanceof ObserverMessage) {
                    observer.dispatchMessage((ObserverMessage) message);
                }
                if(message.getType() == Message.Type.END_OCURRED || message.getType() == Message.Type.TERMINATED){
                    return;
                }
            }
        } catch (IOException ex) {
            observer.dispatchMessage(new TerminatedMessage());
        }
    }

}
