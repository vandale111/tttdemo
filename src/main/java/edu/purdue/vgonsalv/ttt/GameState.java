package edu.purdue.vgonsalv.ttt;

/**
 *
 * @author Victor
 */
public enum GameState {
    RESET, PLAYING, WON, TIE, TERMINATED;
}
