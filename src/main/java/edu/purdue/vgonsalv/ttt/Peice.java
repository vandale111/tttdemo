package edu.purdue.vgonsalv.ttt;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class Peice implements Serializable{
    private static final long serialVersionUID = -3628537149603967608L;
    private final char character;

    public Peice(char character) {
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.character;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Peice other = (Peice) obj;
        if (this.character != other.character) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Peice{" + character + '}';
    }
    
}
