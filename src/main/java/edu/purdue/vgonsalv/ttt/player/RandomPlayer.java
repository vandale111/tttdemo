package edu.purdue.vgonsalv.ttt.player;

import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.Peice;
import edu.purdue.vgonsalv.ttt.actor.Player;
import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.MoveReplyMessage;
import edu.purdue.vgonsalv.ttt.message.MoveRequestMessage;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import edu.purdue.vgonsalv.ttt.Game;

/**
 *
 * @author Victor
 */
public class RandomPlayer extends Player{
    private final Random random;
    private final int delay;
    public RandomPlayer(int delay) {
        random = new Random();
        this.delay = delay;
    }

    @Override
    protected Message makeMove(MoveRequestMessage mrm) throws DisconnectException {
        Game info = mrm.getGame();
        List<Integer> moves = info.getAvailableMoves();
        int move = moves.get(random.nextInt(moves.size()));
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ex) {
            Logger.getLogger(RandomPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new MoveReplyMessage(move);
    }
    
    
    
}
