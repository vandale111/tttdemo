package edu.purdue.vgonsalv.ttt.player;

import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.Game;
import edu.purdue.vgonsalv.ttt.GameState;
import edu.purdue.vgonsalv.ttt.Peice;
import edu.purdue.vgonsalv.ttt.actor.Player;
import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.MoveReplyMessage;
import edu.purdue.vgonsalv.ttt.message.MoveRequestMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Victor
 */
public class MinimaxPlayer extends Player {

    private int maxDepth;
    Random r = new Random();
    @Override
    protected Message makeMove(MoveRequestMessage mrm) throws DisconnectException {
        Game game = mrm.getGame();
        return new MoveReplyMessage(getMove(game));
    }

    private int getMove(Game game) {
        List<Integer> moves = game.getAvailableMoves();
        Peice player = getPeice();

        int best = Integer.MIN_VALUE;
        int bestMove = 0;
        for (Integer move : moves) {
            Game temp = new Game(game);
            temp.makeMove(move, temp.getCurrentPlayerIndex());
            int val = minimax(player, temp);
//            if (val == 10) {
//                return move;
//            }
            if (val > best) {
                best = val;
                bestMove = move;
            }else if(val == best && r.nextBoolean()){
                bestMove = move;
            }
        }
        System.out.println(best);
        return bestMove;
    }

    private int minimax(Peice player, Game game) {
        GameState state = game.getState();
        if (state == GameState.WON) {
            if (game.getWinner() == player) {
                return 10;
            } else {
                return -10;
            }
        } else if (state == GameState.TIE) {
            return 0;
        }
        List<Integer> moves = game.getAvailableMoves();
        final boolean maximizer = player == game.getCurrentPlayer();
        int best = (maximizer ? Integer.MIN_VALUE : Integer.MAX_VALUE);
        for (Integer move : moves) {
            Game temp = new Game(game);
            if (!temp.makeMove(move, temp.getCurrentPlayerIndex())) {
                throw new RuntimeException();
            }
            int val = minimax(player, temp);
            if (maximizer) {
                best = Math.max(best, val);
            } else {
                best = Math.min(best, val);
            }
        }
        if (best > 0) {
            return best - 1;

        } else if (best < 0) {
            return best + 1;
        }
        return best;
    }
}
