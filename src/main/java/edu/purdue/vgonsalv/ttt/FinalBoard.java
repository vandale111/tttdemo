package edu.purdue.vgonsalv.ttt;

import java.util.Collections;

/**
 *
 * @author Victor
 */
public class FinalBoard extends Board {

    private static final long serialVersionUID = 2673008845144466079L;


    public FinalBoard(Board board) {
        super( Collections.unmodifiableList(board.getPeices()), board.getEmptyPeice(),board.getSize());
    }

    @Override
    void setPeice(int pos, Peice peice) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return super.equals(obj);
    }
}
