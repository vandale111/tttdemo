package edu.purdue.vgonsalv.ttt.actor;

import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.message.EndOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.MoveOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.ObserverMessage;
import edu.purdue.vgonsalv.ttt.message.ResetMessage;

/**
 *
 * @author Victor
 */
public abstract class Observer {
    public Message dispatchMessage(ObserverMessage message)throws DisconnectException{
        switch(message.getType()){
            case RESET:
                ResetMessage rm = (ResetMessage)message;
                reset(rm);
                break;
            case MOVE_OCURRED:
                MoveOcurredMessage mom = (MoveOcurredMessage) message;
                moveOcurred(mom);
                break;
            case END_OCURRED:
                EndOcurredMessage eom = (EndOcurredMessage) message;
                endOcurred(eom);
                break;
            case TERMINATED:
                terminated();
        }
        return Message.EMPTY;
    }
    protected void reset(ResetMessage msg) throws DisconnectException{};
    protected void terminated() throws DisconnectException{};
    protected void moveOcurred(MoveOcurredMessage msg) throws DisconnectException{};
    protected void endOcurred(EndOcurredMessage msg)throws DisconnectException{};
}
