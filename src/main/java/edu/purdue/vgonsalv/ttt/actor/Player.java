package edu.purdue.vgonsalv.ttt.actor;

import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.Peice;
import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.MoveReplyMessage;
import edu.purdue.vgonsalv.ttt.message.MoveRequestMessage;
import edu.purdue.vgonsalv.ttt.message.ObserverMessage;
import edu.purdue.vgonsalv.ttt.message.SetPeiceMessage;

/**
 *
 * @author Victor
 */
public abstract class Player extends Observer{
    private Peice peice;
    @Override
    public Message dispatchMessage(ObserverMessage message)throws DisconnectException{
        switch(message.getType()){
            case MOVE_REQUEST:
                MoveRequestMessage mrm = (MoveRequestMessage) message;
                return makeMove(mrm);
            case SET_PEICE:
                SetPeiceMessage spm = (SetPeiceMessage) message;
                peice = spm.getPice();
                break;
            
            default:
                return super.dispatchMessage(message);
        }
        return Message.EMPTY;
    }
    protected  Message makeMove(MoveRequestMessage mrm)throws DisconnectException{
        return Message.EMPTY;
    };
    protected Peice getPeice(){
        return peice;
    }
}
