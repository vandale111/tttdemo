package edu.purdue.vgonsalv.ttt.view;

import edu.purdue.vgonsalv.ttt.Board;
import edu.purdue.vgonsalv.ttt.DisconnectException;
import edu.purdue.vgonsalv.ttt.actor.Observer;
import java.util.StringJoiner;
import edu.purdue.vgonsalv.ttt.message.EndOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.MoveOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.ResetMessage;

/**
 *
 * @author Victor
 */
public class TextView extends Observer {

    @Override
    public void reset(ResetMessage msg) {
        displayBoard(msg.getGame().getBoard());
    }

    @Override
    public void moveOcurred(MoveOcurredMessage msg) {
        displayBoard(msg.getGame().getBoard());
    }

    @Override
    public void endOcurred(EndOcurredMessage msg) {
        if (msg.isWon()) {
            System.out.printf("%c has won!%n", msg.getWinner().getCharacter());
        } else {
            System.out.println("Tie");
        }
    }

    public void displayBoard(Board board) {
        int width = board.getSize() * 2 - 1;
        StringBuffer rowSep = new StringBuffer(width);
        for (int i = 0; i < width; i++) {
            rowSep.append("-");
        }
        rowSep.append(System.lineSeparator());
        StringJoiner rowJoiner = new StringJoiner(rowSep);
        for (int i = 0; i < board.getSize(); i++) {
            StringJoiner colJoiner = new StringJoiner("|", "", System.lineSeparator());
            for (int j = 0; j < board.getSize(); j++) {
                colJoiner.add("" + board.getPeice(i, j).getCharacter());
            }
            rowJoiner.add(colJoiner.toString());
        }
        System.out.print(rowJoiner.toString());
        System.out.flush();
    }

    @Override
    public void terminated() throws DisconnectException {
        System.out.println("Game Terminated");
    }

}
