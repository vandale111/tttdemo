package edu.purdue.vgonsalv.ttt;

import edu.purdue.vgonsalv.ttt.actor.Observer;
import edu.purdue.vgonsalv.ttt.actor.Player;
import edu.purdue.vgonsalv.ttt.message.EndOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.Message;
import edu.purdue.vgonsalv.ttt.message.MoveOcurredMessage;
import edu.purdue.vgonsalv.ttt.message.MoveReplyMessage;
import edu.purdue.vgonsalv.ttt.message.MoveRequestMessage;
import edu.purdue.vgonsalv.ttt.message.ObserverMessage;
import edu.purdue.vgonsalv.ttt.message.ResetMessage;
import edu.purdue.vgonsalv.ttt.message.SetPeiceMessage;
import edu.purdue.vgonsalv.ttt.message.TerminatedMessage;
import edu.purdue.vgonsalv.ttt.remote.RemoteObserver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Victor
 */
public class GameController {

    private static final Peice X = new Peice('X');
    private static final Peice O = new Peice('O');
    private static final Peice EMPTY = new Peice(' ');

    private static final int SIZE = 3;
    private final Game game;

    private final List<Player> players;
    private final List<Observer> observers;

    public void addObserver(RemoteObserver obs) {
        synchronized (observers) {
            observers.add(obs);
        }
    }

    public Peice getEmptyPeice() {
        return EMPTY;
    }

    public GameController(List<? extends Player> players, List<? extends Observer> observers) {
        if (players.size() != 2) {
            throw new IllegalArgumentException(String.format("List must contain 2 players but has %d", players.size()));
        }
        this.game = new Game(SIZE, EMPTY);
        game.setPlayerPeice(X, O);
        this.players = new ArrayList<>(players);
        this.observers = new ArrayList<>(observers);
        this.observers.addAll(players);
        this.game.reset();
    }

    private void reset() {
        game.reset();
        players.get(0).dispatchMessage(new SetPeiceMessage(X));
        players.get(1).dispatchMessage(new SetPeiceMessage(O));
        synchronized (observers) {
            for (Observer observer : observers) {
                observer.dispatchMessage(new ResetMessage(game));
            }
        }
    }

    private void sendToAllObserver(ObserverMessage msg) {
        synchronized (observers) {
            for (Iterator<Observer> it = observers.iterator(); it.hasNext();) {
                Observer observer = it.next();
                try {
                    observer.dispatchMessage(msg);
                } catch (DisconnectException e) {
                    it.remove();
                }
            }
        }
    }

    public boolean playGame() {
        if (game.getState() != GameState.RESET) {
            return false;
        }
        reset();

        do {
            final int currentPlayerIndex = game.getCurrentPlayerIndex();
            final Player currentPlayer = players.get(currentPlayerIndex);
            int move;
            try {
                do {
                    Message reply;
                    do {
                        reply = currentPlayer.dispatchMessage(new MoveRequestMessage(game));
                    } while (reply.getType() != Message.Type.MOVE_REPLY);
                    MoveReplyMessage mrm = (MoveReplyMessage) reply;
                    move = mrm.getMove();
                } while (!game.makeMove(move, currentPlayerIndex));
            } catch (DisconnectException e) {
                sendToAllObserver(new TerminatedMessage());
                return false;
            }

            sendToAllObserver(new MoveOcurredMessage(move, game.getPlayers().get(currentPlayerIndex), game));
        } while (game.getState() == GameState.PLAYING);
        sendToAllObserver(new EndOcurredMessage(game));
        return true;

    }
}
