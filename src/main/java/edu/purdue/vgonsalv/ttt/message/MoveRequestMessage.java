package edu.purdue.vgonsalv.ttt.message;

import java.io.Serializable;
import edu.purdue.vgonsalv.ttt.Game;

/**
 *
 * @author Victor
 */
public class MoveRequestMessage extends PlayerMessage{

    private static final long serialVersionUID = 3467774472340749360L;
    
    public <T extends Serializable> MoveRequestMessage(Game info) {
        super(Type.MOVE_REQUEST, info);
    }

    public Game getGame(){
        return (Game) getData().get(0);
    }
    
}
