package edu.purdue.vgonsalv.ttt.message;

/**
 *
 * @author Victor
 */
public class MoveReplyMessage extends PlayerMessage{
    
    private static final long serialVersionUID = -8649560645711214920L;
    
    public MoveReplyMessage(int move){
        super(Type.MOVE_REPLY, move);
    }
    public int getMove(){
        return (int) getData().get(0);
    }
}
