package edu.purdue.vgonsalv.ttt.message;

import edu.purdue.vgonsalv.ttt.Peice;
import edu.purdue.vgonsalv.ttt.message.Message.Type;
import edu.purdue.vgonsalv.ttt.Game;

/**
 *
 * @author Victor
 */
public class MoveOcurredMessage extends ObserverMessage{

    private static final long serialVersionUID = 5138147283572360392L;
    public MoveOcurredMessage(int pos, Peice peice, Game info){
        super(Type.MOVE_OCURRED,pos,peice,info);
    }
    
    public int getPosition(){
        return (Integer)getData().get(0);
    }
    public Peice getPeice(){
        return (Peice) getData().get(1);
    }
    public Game getGame(){
        return (Game) getData().get(2);
    }
}
