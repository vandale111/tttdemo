package edu.purdue.vgonsalv.ttt.message;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class PlayerMessage extends ObserverMessage{

    private static final long serialVersionUID = -8353343498138348340L;
    
    public <T extends Serializable> PlayerMessage(Type type, T... data) {
        super(type, data);
    }
    
}
