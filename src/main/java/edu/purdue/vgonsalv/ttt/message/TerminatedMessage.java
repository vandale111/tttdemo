package edu.purdue.vgonsalv.ttt.message;

/**
 *
 * @author Victor
 */
public class TerminatedMessage extends ObserverMessage{
    
    private static final long serialVersionUID = 887472037867415476L;
    public TerminatedMessage(){
        super(Type.TERMINATED);
    }
}
