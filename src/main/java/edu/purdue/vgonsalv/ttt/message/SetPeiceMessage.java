package edu.purdue.vgonsalv.ttt.message;

import edu.purdue.vgonsalv.ttt.Peice;

/**
 *
 * @author Victor
 */
public class SetPeiceMessage extends PlayerMessage{

    private static final long serialVersionUID = -7951128581436663802L;
    public SetPeiceMessage(Peice peice){
        super(Type.SET_PEICE,peice);
    }
    public Peice getPice(){
        return (Peice) getData().get(0);
    }
}
