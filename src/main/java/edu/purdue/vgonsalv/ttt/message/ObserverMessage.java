package edu.purdue.vgonsalv.ttt.message;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public abstract class  ObserverMessage extends Message {    

    private static final long serialVersionUID = -3618523352530921625L;
    public <T extends Serializable> ObserverMessage(Type type, T... data) {
        super(type, data);
    }    
}
