package edu.purdue.vgonsalv.ttt.message;

import edu.purdue.vgonsalv.ttt.Peice;
import edu.purdue.vgonsalv.ttt.Game;
import edu.purdue.vgonsalv.ttt.GameState;

/**
 *
 * @author Victor
 */
public class EndOcurredMessage extends ObserverMessage{

    private static final long serialVersionUID = 5036480851989159332L;
    public EndOcurredMessage( Game game){
        super(Type.END_OCURRED,game);
    }
    
    public boolean isWon(){
        return getGame().getState() == GameState.WON;
    }
    public boolean isTie(){
        return getGame().getState() == GameState.TIE;
    }
    public Peice getWinner(){
        return getGame().getWinner();
    }
    public Game getGame(){
        return (Game) getData().get(0);
    }
}
