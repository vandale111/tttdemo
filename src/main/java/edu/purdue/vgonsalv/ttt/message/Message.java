package edu.purdue.vgonsalv.ttt.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Victor
 */
public  class Message implements Serializable{

    private static final long serialVersionUID = -5734246060079847263L;
    public static Message EMPTY = new Message(Type.EMPTY);
    public static enum Type{
        MOVE_REQUEST,SET_PEICE,RESET,MOVE_OCURRED,END_OCURRED,MOVE_REPLY, TERMINATED,EMPTY
    }
    
    private final Type type;
    private final List<Serializable> data;

    protected <T extends Serializable>Message(Type type, T ... data) {
        this.type = type;
        this.data = Collections.unmodifiableList(new ArrayList<>(Arrays.asList(data)));
    }

    public Type getType() {
        return type;
    }

    protected List<Serializable> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Message{" + "type=" + type + ", data=" + data + '}';
    }
    
    
}
