package edu.purdue.vgonsalv.ttt.message;

import edu.purdue.vgonsalv.ttt.Game;

/**
 *
 * @author Victor
 */
public class ResetMessage extends ObserverMessage{

    private static final long serialVersionUID = 6409384124845261760L;

    public ResetMessage(Game info) {
        super(Type.RESET,info);
    }
    public Game getGame(){
        return (Game) getData().get(0);
    }
}
