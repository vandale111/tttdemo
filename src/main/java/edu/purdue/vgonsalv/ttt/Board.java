package edu.purdue.vgonsalv.ttt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Victor
 */
public class Board implements Serializable{
    private static final long serialVersionUID = 4207741766474181987L;
    private final List<Peice> peices;
    private final Peice emptyPeice;
    private final int size;
    public Board(int size, Peice emptyPeice) {
        if(size < 0){
            throw new IllegalArgumentException(String.format("size must be positive: %d", size));
        }
        this.size = size;
        peices = new ArrayList<>(size * size);
        this.emptyPeice = emptyPeice;
        for (int i = 0; i < size*size; i++) {
            peices.add(emptyPeice);
        }
    }

    public Board(Board board) {
        this.emptyPeice = board.emptyPeice;
        this.peices = new ArrayList<>(board.peices);
        this.size = board.size;
    }

    protected Board(List<Peice> peices, Peice emptyPeice, int size) {
        this.peices = peices;
        this.emptyPeice = emptyPeice;
        this.size = size;
    }
    
    public Peice getPeice(int row,int col){
        return getPeice(row*size + col);
    }
    public Peice getPeice(int pos){
        return peices.get(pos);
    }
    void setPeice(int pos, Peice peice){
        peices.set(pos, peice);
    }
    List<Peice> getPeices() {
        return Collections.unmodifiableList(peices);
    }
    
    public int getSize() {
        return size;
    }

    public Peice getEmptyPeice() {
        return emptyPeice;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Board)) {
            return false;
        }
        final Board other = (Board) obj;
        if (this.size != other.size) {
            return false;
        }
        return Objects.equals(this.peices, other.peices);
    }

    @Override
    public String toString() {
        return "Board{" + "peices=" + peices + ", emptyPeice=" + emptyPeice + ", size=" + size + '}';
    }
    
    
}
