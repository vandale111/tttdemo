package edu.purdue.vgonsalv.ttt;

/**
 *
 * @author Victor
 */
public class DisconnectException extends RuntimeException{
    
    private static final long serialVersionUID = 239750341494812112L;

    public DisconnectException() {
    }

    public DisconnectException(String string) {
        super(string);
    }

    public DisconnectException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public DisconnectException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
